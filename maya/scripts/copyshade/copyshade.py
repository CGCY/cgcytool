import sys
from PySide2.QtWidgets import (
    QWidget, QLineEdit, QPushButton, QApplication, QHBoxLayout,
    QLabel, QVBoxLayout
)
import PySide2.QtCore as QtCore
import maya.OpenMayaUI
from shiboken2 import wrapInstance
import maya.cmds as mc


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class CopyShadeWin(QWidget):

    def __init__(self, parent=None):
        super(CopyShadeWin, self).__init__(parent)

        self.initUI()
        self.buttonCommand()

        self.setWindowFlags(QtCore.Qt.Window)

    def initUI(self):

        layouRoot = QVBoxLayout()
        layou1 = QHBoxLayout()
        layou2 = QHBoxLayout()

        self.lab1 = QLabel("get Shade Name:")
        self.linEdit1 = QLineEdit()
        self.btn1 = QPushButton("get Shade")
        self.btn1_set = QPushButton("set Shade")

        layou1.addWidget(self.lab1)
        layou1.addWidget(self.linEdit1)
        layou1.addWidget(self.btn1)
        layou1.addWidget(self.btn1_set)

        self.lab2 = QLabel("get Disp Name:")
        self.linEdit2 = QLineEdit()
        self.btn2 = QPushButton("get Disp")
        self.btn2_set = QPushButton("set Disp")

        layou2.addWidget(self.lab2)
        layou2.addWidget(self.linEdit2)
        layou2.addWidget(self.btn2)
        layou2.addWidget(self.btn2_set)

        layouRoot.addLayout(layou1)
        layouRoot.addLayout(layou2)

        self.setLayout(layouRoot)
        self.setWindowTitle("hcFZ5y7 Copy Objct Shade Tool")
        self.setObjectName("copytool")

    def buttonCommand(self):
        self.btn1.clicked.connect(self.getSelShader)
        self.btn1_set.clicked.connect(self.setSelShader)
        self.btn2.clicked.connect(self.getSelDsp)
        self.btn2_set.clicked.connect(self.setSelDsp)

    def getSelShader(self):
        sel = mc.ls(selection=True)[0]
        shape = mc.listRelatives(
            sel, shapes=True)
        shadingEngine = mc.listConnections(
            shape, source=False, destination=True)
        material = mc.listConnections(
            shadingEngine, source=True, destination=False)
        self.linEdit1.setText(material[0])

    def setSelShader(self):
        copySel = mc.ls(selection=True)
        copyShape = mc.listRelatives(copySel, shapes=True)

        shaderList = list()
        for i in copyShape:
            copyShadingEngine = mc.listConnections(
                i, source=False, destination=True)
            shaderList.append(copyShadingEngine[0])

        shader = self.linEdit1.text()
        for SG in shaderList:
            mc.defaultNavigation(
                ce=1, source=shader, destination="%s.surfaceShader" % SG)

    def getSelDsp(self):
        from pymel.core import listConnections
        copySel = mc.ls(selection=True)[0]
        copyShape = mc.listRelatives(copySel, shapes=True)

        copyShadingEngine = mc.listConnections(
            copyShape, source=False, destination=True)

        material = listConnections(
            copyShadingEngine, source=True, destination=False)[-1]

        if material.type() == "displacementShader":
            self.linEdit2.setText(material.name())

    def setSelDsp(self):
        copySel = mc.ls(selection=True)
        copyShape = mc.listRelatives(copySel, shapes=True)

        shaderList = list()
        for i in copyShape:
            copyShadingEngine = mc.listConnections(
                i, source=False, destination=True)
            shaderList.append(copyShadingEngine[0])

        shader = self.linEdit2.text()
        for SG in shaderList:
            mc.defaultNavigation(
                ce=1, source=shader, destination="%s.displacementShader" % SG)


def showCopyShadeWin():
    if mc.window("copytool", ex=True):
        mc.deleteUI("copytool")

    run = CopyShadeWin(getMainWindowPtr())
    run.show()
