#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import json
import os
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QWidget, QMessageBox
from functools import partial
import PySide2.QtCore as QtCore
import maya.OpenMayaUI
from shiboken2 import wrapInstance
import maya.cmds as mc
from qdarkstyle import style
reload(style)


osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "copy_contr_window.ui")


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class copyHandAttr(QWidget):

    def __init__(self, getMain, parent=None):

        super(copyHandAttr, self).__init__(parent)
        # if mc.window("copycontr_win", ex=1):
        #     mc.deleteUI("copycontr_win")

        self.ui = QUiLoader().load(uipath, getMain)
        self.ui.setObjectName("copycontr_win")

        self.buttonConnect()
        self.workpath = mc.workspace(q=True, fullName=True)

        self.ui.setStyleSheet(style.qss)

        # set window? unclear meaning.But must have
        self.ui.setWindowFlags(QtCore.Qt.Window)
        self.ui.show()

    def getContr(self):
        '''
        Get left and right hand controllers
        '''
        self.contr = list()
        self.sel_contr = mc.ls(sl=True)[0]
        about = self.sel_contr.rsplit("_", 1)[1]
        nameCtrl = self.sel_contr.split(":")[0]
        CHFingerShape = mc.ls(
            "{0}:FK*Finger*_{1}*".format(nameCtrl, about),
            type="nurbsCurve")
        for con in CHFingerShape:
            conParent = mc.listRelatives(con, p=True)
            self.contr.append(conParent[0])

        self.ctrlTranslate = dict()
        for item in self.contr:
            attrName = "{}:" + item[:-1].split(":")[1] + "{}"
            self.ctrlTranslate[attrName+".translate"] = mc.getAttr(
                item+".translate")
            self.ctrlTranslate[attrName+".rotate"] = mc.getAttr(item+".rotate")
            self.ctrlTranslate[attrName+".scale"] = mc.getAttr(item+".scale")

        Fingers = "{0}:Fingers_{1}".format(nameCtrl, about)
        FingersAttr = mc.listAttr(Fingers, ud=True)
        for f in FingersAttr:
            attrName = "{}:Fingers_{}.%s" % f.rsplit(".", 1)[0]
            self.ctrlTranslate[attrName] = float("%.1f" % mc.getAttr(
                "{0}.{1}".format(Fingers, f)))

    def export_connect(self):
        '''
        export json file Path
        '''
        basicFilter = " .json *"
        export = mc.fileDialog2(
            fileFilter=basicFilter, dialogStyle=2,
            cap="import join", dir=self.workpath)
        if export:
            path = export[0]
            self.ui.exportname.setText(path)

    def export_json(self):
        '''
        export json file
        '''
        try:
            self.getContr()
        except BaseException:
            QMessageBox.question(
                self, "Error", "选择一个控制器", QMessageBox.Ok)

        path = self.ui.exportname.text()
        if path:
            if os.path.isfile(path):
                fileQuse = QMessageBox.question(
                    self, "Error", "已经包含名为{}的文件\n是否覆盖".format(
                        os.path.split(path)[1]))
                if fileQuse == QMessageBox.Yes:
                    with open(path, "w") as w:
                        json.dump(self.ctrlTranslate, w)
                    QMessageBox.question(self, "jsonExport", "文件已覆盖")
            else:
                with open(path, "w") as w:
                    json.dump(self.ctrlTranslate, w)
                QMessageBox.question(
                    self, "jsonExport", "json文件导出成功", QMessageBox.Ok)
        else:
            QMessageBox.question(
                self, "Error", "请选择文件路径", QMessageBox.Ok)

    def import_connect(self):
        '''
        import json file Path
        '''
        basicFilter = " .json *"
        export = mc.fileDialog2(
            fileFilter=basicFilter, dialogStyle=2,
            cap="import join", dir=self.workpath)
        if export:
            path = export[0]
            self.ui.importname.setText(path)

    def import_json(self):
        '''
        import json file set Contrl Attr
        '''
        try:
            self.getContr()
        except BaseException:
            QMessageBox.question(
                self, "Error", "选择一个控制器", QMessageBox.Ok)
        path = self.ui.importname.text()
        if path:
            try:
                with open(path, "r") as f:
                    data = json.load(f)

                selContr = mc.ls(sl=True)[0]
                about = selContr.rsplit("_", 1)[1]
                name = selContr.split(":")[0]
                for value, key in data.items():
                    contrlName = value.format(name, about)
                    if isinstance(key, (list)):
                        print(contrlName)
                        mc.setAttr(contrlName, key[0][0], key[0][1], key[0][2])
                    else:
                        print(contrlName)
                        mc.setAttr(contrlName, key)
                QMessageBox.question(
                    self, "jsonImport", "文件导入成功", QMessageBox.Ok)
            except BaseException:
                QMessageBox.question(
                    self, "Error", "文件导入失败", QMessageBox.Ok)

    def copyAttr(self):
        '''
        copy finger Attr
        '''
        try:
            self.getContr()
            selContr = mc.ls(sl=True)[1]
            about = selContr.rsplit("_", 1)[1]
            name = selContr.split(":")[0]
            for value, key in self.ctrlTranslate.items():
                contrlName = value.format(name, about)
                if isinstance(key, (list)):
                    mc.setAttr(contrlName, key[0][0], key[0][1], key[0][2])
                else:
                    mc.setAttr(contrlName, key)
        except BaseException:
            QMessageBox.question(
                    self, "Error",
                    "需要选择手A的一个控制器选择手B的一个控制器",
                    QMessageBox.Ok)

    def buttonConnect(self):
        self.ui.Exportpath.clicked.connect(partial(self.export_connect))
        self.ui.importtpath.clicked.connect(partial(self.import_connect))
        self.ui.CROAT.clicked.connect(partial(self.copyAttr))
        self.ui.EXBUTON.clicked.connect(partial(self.export_json))
        self.ui.IMBUTON.clicked.connect(partial(self.import_json))


def showCfxWin():
    if mc.window("copycontr_win", ex=True):
        mc.deleteUI("copycontr_win")
    run = copyHandAttr(getMainWindowPtr())
