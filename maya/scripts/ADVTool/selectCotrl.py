#!/usr/bin/python
# -*- coding: UTF-8 -*-

import maya.cmds as mc


def selectLevel(moder):
    """
    selectLevel("lower"  of "uper")
    Select the controller according to the serial number
    """
    for contrcurve in mc.ls(sl=1):
        strContrcurve = contrcurve.rsplit("_", 1)
        allnumber = "{}*{}".format(strContrcurve[0][:-1], strContrcurve[1])
        number = strContrcurve[0][-1]
        allcont = mc.ls(allnumber, type="transform")
        selectlower = "{0}{2}_{1}".format(
            strContrcurve[0][:-1], strContrcurve[1], (int(number)-1))
        selectuper = "{0}{2}_{1}".format(
            strContrcurve[0][:-1], strContrcurve[1], (int(number)+1))
        selectuper_d = "{0}{2}_{1}".format(
            strContrcurve[0][:-1], strContrcurve[1], (int(number)))
        if moder == "lower":
            if selectlower in allcont:
                mc.select(selectlower, add=True)
        elif moder == "uper":
            if selectuper in allcont:
                mc.select(selectuper_d, d=True)
                mc.select(selectuper, add=True)


def selecthandcontroller():
    """
    Select all hand controllers based on the namespace
    """
    controller = mc.ls(sl=1)[0]
    namespace = controller.split(":")[0]
    CHFingerShape = mc.ls(
        "{0}:FK*Finger*".format(namespace),  type="nurbsCurve")
    CHFinger = list()
    mc.select(cl=True)
    for con in CHFingerShape:
        conParent = (mc.listRelatives(con, p=True))
        CHFinger.append(conParent[0])
    mc.select(CHFinger)
