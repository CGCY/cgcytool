# ! -*- coding:utf -8 -*-
# author：liubingdong
# date：20190427


import maya.cmds as mc
import maya.mel as mel
# 一对多拷贝权重


def copySkin():
    allElement = mc.ls(sl=1)
    for a in range(1, len(allElement)):
        canKao = allElement[0]
        currentObj = allElement[a]
        skinnode = mel.eval(' findRelatedSkinCluster  %s' % canKao)

        if skinnode:
            canKaoGuge = mc.skinCluster(skinnode, q=1, inf=1)
            skinnode_new = mel.eval(' findRelatedSkinCluster  %s' % currentObj)
            if skinnode_new:
                zuiZhongGuGe = mc.skinCluster(skinnode_new, q=1, inf=1)
                if zuiZhongGuGe and canKaoGuge:
                    if canKaoGuge != zuiZhongGuGe:
                        newNeedAdd = []
                        for eachList in canKaoGuge:
                            if eachList not in zuiZhongGuGe:
                                newNeedAdd.append(eachList)
                        if newNeedAdd != []:
                            for eachNeedAdd in newNeedAdd:
                                mc.skinCluster(
                                    skinnode_new, e=1, lw=1, wt=0,
                                    ai=eachNeedAdd)
            else:
                # 蒙皮
                mc.skinCluster(canKaoGuge, currentObj, rui=0, tsb=True)
            # 拷贝权重
            mc.select(cl=1)
            mc.select(canKao)
            mc.select(currentObj, add=1)
            mel.eval('''copySkinWeights
              -noMirror -surfaceAssociation closestPoint
              -influenceAssociation oneToOne -influenceAssociation oneToOne
              -influenceAssociation oneToOne;''')
            print('\n已经拷贝.............', currentObj)
