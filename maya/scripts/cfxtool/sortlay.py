import maya.cmds as mc
from maya.mel import eval


def SoftDisplayLayer():
    DisplayLayer = mc.ls(type="displayLayer")[1:]
    DisplayLayer.reverse()
    for n, lay in enumerate(DisplayLayer, start=1):
        mc.setAttr("%s.displayOrder" % lay, n)
    eval("reorderLayers 0")
