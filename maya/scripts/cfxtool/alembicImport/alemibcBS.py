#!/usr/bin/python
# -*- coding: UTF-8 -*-

import maya.cmds as mc


def importBS(alembicPath):
    '''
    Importing ABC cache and merging BS
    '''
    before = set(mc.ls(type="transform"))
    mc.file(alembicPath, i=True)
    after = set(mc.ls(type="transform"))
    alembic = after - before
    Group_ = mc.group(em=True, name="Alembic")
    for item in alembic:
        try:
            bsobj = mc.ls("*:%s" % item, type="transform")[0]
            blends = mc.blendShape(item, bsobj)
            mc.setAttr("{0}.{1}".format(blends[0], item), 1)
            mc.parent(item, Group_)
            mc.rename(item, "_%s" % item)
            print("ABC imported successfully")
        except BaseException:
            exceptGroup = mc.group(em=True, name="except", parent=Group_)
            mc.parent(item, Group_)
            print("The model in question is in the exception group")

    mc.setAttr(Group_+".visibility", 0, l=True)
