import json
import sys
import os
import maya.cmds as mc
from cfxtool.copyattr import getAttr
import getAttr

osfile = os.path.split(__file__)[0]
jsonpath = os.path.join(osfile, "clothattr.json")


def ExportClothAttr(fileName):
    '''
    Export cloth presets to file
    '''
    with open(jsonpath, "r") as f:
        attr = f.readlines()

    attrList = getAttr.getClothAttr()
    clothDict = {}
    for key, number in zip(attr, attrList):
        clothDict[key] = number

    with open(fileName+".json", "w") as f:
        json.dump(clothDict, f)


def ExportDescribe(fileName, Describe):
    with open(fileName+".txt", "w") as f:
        f.write(Describe)
