#!/usr/bin/python
# -*- coding: UTF-8 -*-

import json
import os
import maya.cmds as mc


def getClothAttr():
    '''
    get selected cloth attr
    '''
    sel = mc.ls(sl=1)[0]
    clothAttrList = list()
    clothAttrList.append(mc.getAttr("%s.collide" % sel))
    clothAttrList.append(mc.getAttr("%s.selfCollide" % sel))
    clothAttrList.append(mc.getAttr("%s.collisionFlag" % sel))
    clothAttrList.append(mc.getAttr("%s.selfCollisionFlag" % sel))
    clothAttrList.append(mc.getAttr("%s.collideStrength" % sel))
    clothAttrList.append(mc.getAttr("%s.collisionLayer" % sel))
    clothAttrList.append(mc.getAttr("%s.thickness" % sel))
    clothAttrList.append(mc.getAttr("%s.selfCollideWidthScale" % sel))
    clothAttrList.append(mc.getAttr("%s.bounce" % sel))
    clothAttrList.append(mc.getAttr("%s.friction" % sel))
    clothAttrList.append(mc.getAttr("%s.stickiness" % sel))
    clothAttrList.append(mc.getAttr("%s.collideStrengthMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.thicknessMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.bounceMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.frictionMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.stickinessMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.stretchResistance" % sel))
    clothAttrList.append(mc.getAttr("%s.compressionResistance" % sel))
    clothAttrList.append(mc.getAttr("%s.bendResistance" % sel))
    clothAttrList.append(mc.getAttr("%s.bendAngleDropoff" % sel))
    clothAttrList.append(mc.getAttr("%s.shearResistance" % sel))
    clothAttrList.append(mc.getAttr("%s.restitutionAngle" % sel))
    clothAttrList.append(mc.getAttr("%s.restitutionTension" % sel))
    clothAttrList.append(mc.getAttr("%s.rigidity" % sel))
    clothAttrList.append(mc.getAttr("%s.deformResistance" % sel))
    clothAttrList.append(mc.getAttr("%s.usePolygonShells" % sel))
    clothAttrList.append(mc.getAttr("%s.inputMeshAttract" % sel))
    clothAttrList.append(mc.getAttr("%s.inputAttractMethod" % sel))
    clothAttrList.append(mc.getAttr("%s.inputAttractDamp" % sel))
    clothAttrList.append(mc.getAttr("%s.restLengthScale" % sel))
    clothAttrList.append(mc.getAttr("%s.bendAngleScale" % sel))
    clothAttrList.append(mc.getAttr("%s.pointMass" % sel))
    clothAttrList.append(mc.getAttr("%s.lift" % sel))
    clothAttrList.append(mc.getAttr("%s.drag" % sel))
    clothAttrList.append(mc.getAttr("%s.tangentialDrag" % sel))
    clothAttrList.append(mc.getAttr("%s.damp" % sel))
    clothAttrList.append(mc.getAttr("%s.stretchDamp" % sel))
    clothAttrList.append(mc.getAttr("%s.scalingRelation" % sel))
    clothAttrList.append(mc.getAttr("%s.ignoreSolverGravity" % sel))
    clothAttrList.append(mc.getAttr("%s.ignoreSolverWind" % sel))
    clothAttrList.append(mc.getAttr("%s.localForceX" % sel))
    clothAttrList.append(mc.getAttr("%s.localForceY" % sel))
    clothAttrList.append(mc.getAttr("%s.localForceZ" % sel))
    clothAttrList.append(mc.getAttr("%s.localWindX" % sel))
    clothAttrList.append(mc.getAttr("%s.localWindY" % sel))
    clothAttrList.append(mc.getAttr("%s.localWindZ" % sel))
    clothAttrList.append(mc.getAttr("%s.stretchMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.compressionMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.bendMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.bendAngleDropoffMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.restitutionAngleMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.rigidityMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.deformMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.inputAttractMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.restLengthScaleMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.dampMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.massMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.liftMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.dragMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.tangentialDragMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.wrinkleMapType" % sel))
    clothAttrList.append(mc.getAttr("%s.wrinkleMapScale" % sel))
    clothAttrList.append(mc.getAttr("%s.maxIterations" % sel))
    clothAttrList.append(mc.getAttr("%s.maxSelfCollisionIterations" % sel))
    return clothAttrList


def setClothAttr(preFile):
    '''
    set selected cloth attr
    '''
    sels = mc.ls(sl=True)
    dateload = {}
    with open(preFile, "r") as r:
        dateload = json.load(r)

    for sel in sels:
        for key, value in dateload.items():
            attrName = str(key).split("\r")[0].split(",")[0][1:-1]
            mc.setAttr("{0}.{1}".format(sel, attrName), value)


def getDescribe(jsonFile):
    Describe = os.path.splitext(jsonFile)[0]+".txt"
    if os.path.isfile(Describe):
        with open(Describe, "r") as f:
            txt = f.read()
            return txt


def copyClothAttr():
    '''
    copy selected Attr
    '''

    attrList = getClothAttr()
    copysel = mc.ls(sl=True)[1:]

    osfile = os.path.split(__file__)[0]
    jsonpath = os.path.join(osfile, "clothattr.json")
    with open(jsonpath, "r") as f:
        attrDict = f.readlines()

    for sel in copysel:
        for key, attr in zip(attrDict, attrList):
            attrName = str(key).split("\r")[0].split(",")[0][1:-1]
            mc.setAttr("%s.%s" % (sel, attrName), attr)


def copyNHairAttr():
    '''
    copy selected nHair attr
    '''
    osfile = os.path.split(__file__)[0]
    jsonpath = os.path.join(osfile, "nhair.json")

    with open(jsonpath, "r") as f:
        attrDict = f.read().splitlines()
    sel = mc.ls(sl=True)[0]
    hairAttr = list()
    for att in attrDict:
        hairAttr.append(mc.getAttr("{}.{}".format(sel, att)))

    copysel = mc.ls(sl=True)[1:]
    for obj in copysel:
        for key, value in zip(attrDict, hairAttr):
            mc.setAttr("{}.{}".format(obj, key), value)


def copyNHairattractionScale(scaleType):
    '''
    Copy the attractionscale or stiffness scale of the hair system

    copyNHairattractionScale("attractionScale" of "stiffnessScale")
    '''

    sel = mc.ls(sl=True)[0]
    acts = mc.ls("%s.%s[0:]" % (sel, scaleType))
    acts_Position = list()
    acts_FloatValue = list()
    acts_Interp = list()
    for i in acts:
        acts_Position.append(mc.getAttr("%s.%s_Position" % (i, scaleType)))
        acts_FloatValue.append(mc.getAttr("%s.%s_FloatValue" % (i, scaleType)))
        acts_Interp.append(mc.getAttr("%s.%s_Interp" % (i, scaleType)))

    copysel = mc.ls(sl=True)[1:]
    for i in copysel:

        for atNumber, attr in enumerate(acts_Position, start=0):
            mc.setAttr(
                "{0}.{2}[{1}].{2}_Position".format(
                    i, atNumber, scaleType), attr)

        for atNumber, attr in enumerate(acts_FloatValue, start=0):
            mc.setAttr(
                "{0}.{2}[{1}].{2}_FloatValue".format(
                    i, atNumber, scaleType), attr)

        copyacts = mc.ls("%s.%s[0:]" % (i, scaleType))

        for copyact, mod in zip(copyacts, acts_Interp):
            mc.setAttr("{0}.{1}_Interp".format(copyact, scaleType), mod)

        for r in copyacts[len(acts):]:
            mc.removeMultiInstance(r, b=True)
