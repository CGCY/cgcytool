import random
import maya.cmds as mc


def setShadeColor(color):
    '''
    Set the specified color
    '''
    sel = mc.ls(sl=True)
    lambert = mc.shadingNode("lambert", asShader=True)
    lambertSG = mc.sets(
        renderable=True,
        noSurfaceShader=True, empty=True, name="%sSG" % lambert)
    mc.connectAttr("%s.outColor" % lambert, "%s.surfaceShader" % lambertSG)

    for i in sel:
        mc.sets(i, e=True, forceElement=lambertSG)

    mc.setAttr(
        "%s.color" % lambert, color[0], color[1], color[2], type="double3")


def randomColor():
    '''
    Set a random color for each object
    '''
    sel = mc.ls(sl=1)
    for i in sel:
        lambert = mc.shadingNode("lambert", asShader=True)
        lambertSG = mc.sets(
            renderable=True,
            noSurfaceShader=True, empty=True, name="%sSG" % lambert)
        mc.connectAttr("%s.outColor" % lambert, "%s.surfaceShader" % lambertSG)

        mc.sets(i, e=True, forceElement=lambertSG)
        radomcolor = [
            "%.2f" % random.random(), "%.1f" % random.random(),
            "%.2f" % random.random()]
        mc.setAttr(
            "%s.color" % lambert, float(radomcolor[0]), float(radomcolor[1]),
            float(radomcolor[2]), type="double3")


def CreateColorramp():
    '''
    Create a red-green gradient
    '''
    sel = mc.ls(sl=True)
    for i in sel:
        # Create lambert node
        lambert = mc.shadingNode("lambert", asShader=True)
        lambertSG = mc.sets(
            renderable=True,
            noSurfaceShader=True, empty=True, name="%sSG" % lambert)
        mc.connectAttr("%s.outColor" % lambert, "%s.surfaceShader" % lambertSG)
        mc.sets(i, e=True, forceElement=lambertSG)

        # Create ramp node
        ramp = mc.shadingNode("ramp", asTexture=True)
        rampTexture = mc.shadingNode("place2dTexture", asUtility=True)
        mc.connectAttr("%s.outUV" % rampTexture, "%s.uvCoord" % ramp)
        mc.connectAttr(
            "%s.outUvFilterSize" % rampTexture, "%s.uvFilterSize" % ramp)

        mc.setAttr(
            "%s.colorEntryList[0].color" % ramp, 1, 0, 0, type="double3")
        mc.setAttr(
            "%s.colorEntryList[1].color" % ramp, 0, 1, 0, type="double3")
        mc.setAttr(
            "%s.colorEntryList[1].position" % ramp, 1)

        mc.connectAttr("%s.outColor" % ramp, "%s.color" % lambert)
