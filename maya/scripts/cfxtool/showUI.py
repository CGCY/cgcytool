#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import os
import shutil
from PySide2.QtUiTools import QUiLoader
from PySide2.QtGui import QIntValidator, QIcon
from PySide2.QtWidgets import (
    QListWidgetItem, QWidget, QMenu, QPushButton, QFileDialog, QMessageBox)
import PySide2.QtCore as QtCore
import maya.OpenMayaUI
from shiboken2 import wrapInstance
import maya.cmds as mc
from functools import partial
from maya.mel import eval
import cfxtool.sortlay as sortlay
import cfxtool.setobjectColor as setobjectColor
import cfxtool.simMark as simMark
import cfxtool.showNrigid as showNrigid
import cfxtool.copySkinWeight as copySkinWeight
from cfxtool.copyattr import saveAttr
from cfxtool.copyattr import getAttr
from cfxtool.alembicImport import alemibcBS
from qdarkstyle import style
reload(style)

osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "cfxtool.ui")


eval('source "cfxtool/animHUD.mel"')


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class CfxToolUI(QWidget):

    def __init__(self, getMain, parent=None):
        super(CfxToolUI, self).__init__(parent)
        # load UI File
        self.ui = QUiLoader().load(uipath, getMain)

        # setwindTile as mayafilename
        self.filename = mc.file(q=True, sn=True).split('/')[-1]
        winTitle = " -- bilibili hcFZ5y7"
        self.ui.setWindowTitle(self.filename+winTitle)

        # Set component properties
        self.setUiAttr()
        self.setUITip()

        # set Path
        self.PublicPreinstall = self.ui.PublicPath.text()

        # set Button command
        self.setColorButton()
        self.ui.RefreshList.clicked.connect(partial(self.listRefRoles))
        self.ui.delectCache.clicked.connect(partial(self.deleteCache))
        self.ui.SoftDisplayLayer.clicked.connect(partial(self.sortLayer))
        self.ui.addSign.clicked.connect(partial(self.addMark))
        self.ui.delSign.clicked.connect(partial(self.delMark))
        self.ui.playblastbutton.clicked.connect(partial(self.playBlastOptions))
        self.ui.nRigidPushButton.clicked.connect(partial(
            lambda: self.showRigid("nRigid")))
        self.ui.nclothPushButton.clicked.connect(partial(
            lambda: self.showRigid("nCloth")))
        self.ui.CopySkinButton.clicked.connect(partial(self.copySk))
        self.ui.RefreshPRList.clicked.connect(partial(self.addAllUser))
        self.ui.PullJson.clicked.connect(partial(self.ExportJson))
        self.ui.PushJson.clicked.connect(partial(self.importJson))
        self.ui.copyshapeAttr.clicked.connect(partial(self.copyAttr))
        self.ui.importBS.clicked.connect(partial(self.importBS))

        # set QComboBox command
        self.ui.selectformat.currentIndexChanged.connect(self.switchFormat)

        # Master list
        self.ui.conlist.itemClicked.connect(partial(self.selectItem))
        self.ui.conlist.itemSelectionChanged.connect(partial(self.listClear))

        self.ui.UserNameList.itemClicked.connect(self.userClick)
        self.ui.jsonList.itemClicked.connect(self.setJson)

        # get USER
        self.ui.userNameEdit.setText(os.getenv("USERNAME"))

        # 233333
        # File list
        self.ui.CACHELIST.itemClicked.connect(partial(self.selectCache))
        self.ui.CACHELIST.itemSelectionChanged.connect(partial(self.listClear))
        # 23333

        self.ui.listAll.clicked.connect(partial(self.addItems))

        self.ui.CLOSESOLVES.clicked.connect(partial(self.closeNucleus))
        self.ui.SETSTTIME.clicked.connect(partial(self.setNucleusTime))

        self.ui.DoNcache.clicked.connect(partial(self.CreateNclothCach))

        self.ui.OPRNFILEPATH.clicked.connect(partial(self.openFilewin))
        self.ui.selectplayblastPath.clicked.connect(
            partial(self.slavePlayblast))
        self.ui.alembicPathButton.clicked.connect(
            partial(self.setAlembicFilePath))

        self.ui.RefreshCache.clicked.connect(partial(self.RefreshCachedList))

        self.ui.CopyCache.clicked.connect(partial(self.copyFile))

        self.ui.SETCACHEPATH.clicked.connect(partial(self.setCachePath))

        # set RadioButton clicked
        self.ui.nucleustype.clicked.connect(partial(self.addItems))
        self.ui.nclothtype.clicked.connect(partial(self.addItems))
        self.ui.nahirtype.clicked.connect(partial(self.addItems))

        self.ui.Cachetype.clicked.connect(partial(self.cachelist))

        # hair active Button
        self.ui.activeOff.clicked.connect(partial(self.sethaiswitch))
        self.ui.activeOn.clicked.connect(partial(self.sethaiswitch))

        # add And set hair sim Method Options
        self.addAndSetHairMenu()

        # set hide delectCache button
        self.ui.delectCache.hide()

        # set window? unclear meaning.But must have
        self.ui.setWindowFlags(QtCore.Qt.Window)

        self.ui.show()

    def copyAttr(self):
        '''
        copy selected Attr
        '''
        sel1 = mc.ls(sl=True)
        ntype = None
        for sel in sel1:
            if mc.nodeType(sel) == "nCloth":
                ntype = "nCloth"
            elif mc.nodeType(sel) == "hairSystem":
                ntype = "hairSystem"
            else:
                ntype = None
                print("n")
                break

        reload(getAttr)
        if ntype == "nCloth":
            clothAttr = getAttr.copyClothAttr()

        elif ntype == "hairSystem":
            clothAttr = getAttr.copyNHairAttr()
            getAttr.copyNHairattractionScale("attractionScale")
            getAttr.copyNHairattractionScale("stiffnessScale")

    def setJson(self):
        '''
        Import the JSON file and assign it to the cloth
        '''
        userName = self.ui.UserNameList.selectedItems()[0].text()
        jsonName = self.ui.jsonList.selectedItems()[0].text()
        self.jsonName = os.path.join(
            self.PublicPreinstall, userName, jsonName)
        reload(getAttr)
        txt = getAttr.getDescribe(self.jsonName)
        if getAttr.getDescribe(self.jsonName):
            self.ui.getDescribeTextEdit.setPlainText(txt)
        else:
            self.ui.getDescribeTextEdit.setPlainText("没有描述信息")

    def importJson(self):
        '''
        import json file
        '''
        getAttr.setClothAttr(self.jsonName)

    def ExportJson(self):
        '''
        Export json file
        '''
        userName = self.ui.userNameEdit.text()
        filename = self.ui.PresetEdit.text()
        DescribeText = self.ui.DescribeTextEdit.toPlainText()
        filePath = os.path.join(
            self.PublicPreinstall, userName)
        if not os.path.isdir(filePath):
            os.makedirs(filePath)
        jsonPath = os.path.join(filePath, filename)
        if filename:
            try:
                saveAttr.ExportClothAttr(jsonPath)
                saveAttr.ExportDescribe(jsonPath, DescribeText)
                QMessageBox.question(
                    self, "question", "提交成功?", QMessageBox.Yes)
            except BaseException:
                pass

    def userClick(self):
        '''
        Click the user name to list the files
        '''
        userName = self.ui.UserNameList.selectedItems()[0].text()
        dir = os.listdir(os.path.join(self.PublicPreinstall, userName))
        jsonfileName = list()
        for f in dir:
            if os.path.splitext(f)[1] == ".json":
                jsonfileName.insert(0, f)
        self.ui.jsonList.clear()
        if jsonfileName:
            self.ui.jsonList.addItems(jsonfileName)

    def addAllUser(self):
        '''
        Get user name added to list
        '''
        userList = os.listdir(self.PublicPreinstall)
        self.ui.UserNameList.clear()
        self.ui.jsonList.clear()
        self.ui.UserNameList.addItems(userList)

    def copySk(self):
        '''
        copySkinWeights
        '''
        copySkinWeight.copySkin()

    def addAndSetHairMenu(self):
        offMenu = self.ui.simMethod.addItem("off")
        staticMenu = self.ui.simMethod.addItem("Static")
        dynamicMenu = self.ui.simMethod.addItem("Dynamic Follicles Only")
        allMenu = self.ui.simMethod.addItem("All Follicles")
        self.ui.simMethod.currentIndexChanged.connect(self.sethairAttr)

        if self.ui.simMethod.currentText() == "off":
            pass

    def addMark(self):
        # print("addMark")
        simMark.addMark()

    def delMark(self):
        # print("delMark")
        simMark.delMark()

    def sethaiswitch(self):
        '''
        Set hair active
        '''
        sw = self.ui.activeOn.isChecked()
        hairSel = mc.ls(sl=1, type="hairSystem")
        for i in hairSel:
            mc.setAttr("{}.active".format(i), sw)

    def showRigid(self, colType):
        '''
        Show current solver collision
        '''
        print(colType)
        showNrigid.showNucleusNrigid(colType)

    def sethairAttr(self):
        '''
        Set the hair solution mode
        '''
        print("hello")
        hairSel = mc.ls(sl=1, type="hairSystem")
        attrSet = ["off", "Static", "Dynamic Follicles Only", "All Follicles"]
        hairdir = {}
        for i, x in zip(attrSet, range(5)):
            hairdir[i] = x
        attr = self.ui.simMethod.currentText()
        for i in hairSel:
            mc.setAttr("{}.simulationMethod".format(i), hairdir[attr])

    def selectItem(self, item):
        '''
        refresh the list
        '''
        selctedNumber = [i.text() for i in self.ui.conlist.selectedItems()]
        if len(selctedNumber) <= 1:
            print("select 1", selctedNumber)
            mc.select(cl=True)
            mc.select(item.text())
        elif len(selctedNumber) > 1:
            print("select 1+", selctedNumber)
            mc.select(item.text(), add=True)
        if not item.isSelected() and selctedNumber:
            print("select 1-", selctedNumber)
            mc.select(item.text(), d=True)

    def listClear(self):
        selctedNumber = [i.text() for i in self.ui.conlist.selectedItems()]
        if not selctedNumber:
            mc.select(cl=True)

    def selectCache(self, items):
        '''
        refresh the list
        '''
        selctedNumber = [i.text() for i in self.ui.CACHELIST.selectedItems()]
        if len(selctedNumber) <= 1:
            print("select 1", selctedNumber)
            mc.select(cl=True)
            mc.select(items.text())
        elif len(selctedNumber) > 1:
            print("select 1+", items.text())
            mc.select(selctedNumber)
        if not items.isSelected() and selctedNumber:
            print("select 1-", selctedNumber)
            mc.select(items.text(), d=True)

    def CacheClear(self):
        selctedNumber = [i.text() for i in self.ui.CACHELIST.selectedItems()]
        if not selctedNumber:
            mc.select(cl=True)

    def addItems(self):
        '''
        add maya Node
        '''
        if not self.ui.Cachetype.isChecked():
            # set hide delectCache button
            self.ui.delectCache.hide()
            selectType = ["nCloth", "hairSystem", "nucleus"]
            if self.ui.RolesCom.currentText() == "------------":
                nameSpaces = False
            else:
                nameSpaces = True

            chName = self.ui.RolesCom.currentText()[:-2]
            chChecked = self.ui.TaggingMode.isChecked()
            if self.ui.listAll.isChecked():
                self.ui.conlist.clear()
                for ntype in selectType:
                    if chChecked:
                        if not nameSpaces:
                            self.ui.conlist.addItems(mc.ls(type=ntype))
                        else:

                            self.ui.conlist.addItems(mc.ls(
                                "%s:*" % chName,
                                type=ntype))
                    else:
                        if not nameSpaces:
                            simObject = [
                                i for i in mc.ls(type=ntype)
                                if mc.attributeQuery(
                                    "cfxAttr", node=i, ex=True)]
                            self.ui.conlist.addItems(mc.ls(simObject))
                        else:
                            simObject = [
                                i for i in mc.ls(
                                    "%s:*" % chName, type=ntype)
                                if mc.attributeQuery(
                                    "cfxAttr", node=i, ex=True)]
                            self.ui.conlist.addItems(mc.ls(simObject))
            elif self.ui.nucleustype.isChecked():
                self.ui.conlist.clear()
                if chChecked:
                    if not nameSpaces:
                        self.ui.conlist.addItems(mc.ls(type=selectType[-1]))

                    else:
                        self.ui.conlist.addItems(mc.ls(
                            "%s:*" % chName,
                            type=selectType[-1]))
                else:
                    if not nameSpaces:
                        simObject = [
                            i for i in mc.ls(type=selectType[-1])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)

                    else:
                        simObject = [
                            i for i in mc.ls(
                                "%s:*" % chName, type=selectType[-1])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)
            elif self.ui.nclothtype.isChecked():
                self.ui.conlist.clear()
                if chChecked:
                    if not nameSpaces:
                        self.ui.conlist.addItems(mc.ls(type=selectType[0]))

                    else:
                        self.ui.conlist.addItems(mc.ls(
                            "%s:*" % chName,
                            type=selectType[0]))

                else:
                    if not nameSpaces:
                        simObject = [
                            i for i in mc.ls(type=selectType[0])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)

                    else:
                        simObject = [
                            i for i in mc.ls(
                                "%s:*" % chName, type=selectType[0])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)
            elif self.ui.nahirtype.isChecked():
                self.ui.conlist.clear()
                if chChecked:
                    if not nameSpaces:
                        self.ui.conlist.addItems(mc.ls(type=selectType[1]))

                    else:
                        self.ui.conlist.addItems(mc.ls(
                            "%s:*" % chName,
                            type=selectType[1]))
                else:
                    if not nameSpaces:
                        simObject = [
                            i for i in mc.ls(type=selectType[1])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)

                    else:
                        simObject = [
                            i for i in mc.ls(
                                "%s:*" % chName,
                                type=selectType[1])
                            if mc.attributeQuery(
                                "cfxAttr", node=i, ex=True)]
                        self.ui.conlist.addItems(simObject)
        else:
            self.cachelist()

    def cachelist(self):
        '''
        List all caches
        '''
        self.ui.conlist.clear()
        self.ui.conlist.addItems(mc.ls(type="cacheFile"))
        self.ui.conlist.addItems(mc.ls(type="AlembicNode"))
        self.ui.delectCache.show()

    def RefreshCachedList(self):
        '''
        Refresh the list of cached files
        '''
        self.ui.CACHELIST.clear()
        self.ui.CACHELIST.addItems(mc.ls(type="cacheFile"))
        self.ui.CACHELIST.addItems(mc.ls(type="AlembicNode"))

    def deleteCache(self):
        '''
        Delete the selected Cache
        '''
        cecheNode = mc.ls(sl=1, type="cacheFile")
        cecheNode.append(mc.ls(sl=1, type="AlembicNode"))
        for de in cecheNode:
            mc.delete(de)
        # refresh the list
        self.cachelist()

    def setUiAttr(self):
        '''
        UI display properties
        '''
        self.ui.STARTIME.setValidator(QIntValidator())
        self.ui.setStyleSheet(style.qss)
        logopath = os.path.join(osfile, "logo.png")
        logopath = logopath.replace("\\", "/")
        self.ui.logoButton.setStyleSheet("background-image: url('{0}')".format(
            logopath))
        self.ui.setColorRed.setStyleSheet('background-color: #cc0000')
        self.ui.setColorDarkblue.setStyleSheet('background-color: #070096')
        self.ui.setColorLightgreen.setStyleSheet('background-color: #12ff00')
        self.ui.setColorDarkgreen.setStyleSheet('background-color: #065800')
        self.ui.setColorLightyellow.setStyleSheet('background-color: #e4ff00')
        self.ui.setColorPurple.setStyleSheet('background-color: #9000ff')
        self.ui.setColorLightblue.setStyleSheet('background-color: #00c6ff')
        self.ui.setColorwhite.setStyleSheet('background-color: #ffffff')

    def setColorButton(self):

        self.ui.setColorRed.clicked.connect(partial(
            lambda: self.setColor([0.603831, 0, 0])))
        self.ui.setColorDarkblue.clicked.connect(partial(
            lambda: self.setColor([0.00212416, 0, 0.304995])))
        self.ui.setColorLightgreen.clicked.connect(partial(
            lambda: self.setColor([0.00604871, 1.000024, 0])))
        self.ui.setColorDarkgreen.clicked.connect(partial(
            lambda: self.setColor([0.00182071, 0.0975854, 0])))
        self.ui.setColorLightyellow.clicked.connect(partial(
            lambda: self.setColor([0.775831, 1.000024, 0])))
        self.ui.setColorPurple.clicked.connect(partial(
            lambda: self.setColor([0.278898, 0, 1.000024])))
        self.ui.setColorLightblue.clicked.connect(partial(
            lambda: self.setColor([0, 0.564708, 1.000024])))
        self.ui.setColorwhite.clicked.connect(partial(
            lambda: self.setColor([1, 1, 1])))
        self.ui.randomColorButton.clicked.connect(partial(self.randomColor))
        self.ui.colorRampButton.clicked.connect(partial(self.colorRamp))

    def setColor(self, color):
        setobjectColor.setShadeColor(color)

    def randomColor(self):
        setobjectColor.randomColor()

    def colorRamp(self):
        setobjectColor.CreateColorramp()

    def closeNucleus(self):
        '''
        close other nucleus
        '''
        sel = mc.ls(sl=1, type="nucleus")
        otherSel = list()
        otherSel = mc.ls(type="nucleus")
        # print(type(sel))
        if sel:
            mc.setAttr("%s.enable" % sel[0], 1)
            otherSel.remove(sel[0])
            for nucleus in otherSel:
                mc.setAttr("%s.enable" % nucleus, 0)
        else:
            for nucleus in otherSel:
                mc.setAttr("%s.enable" % nucleus, 0)

    def setNucleusTime(self):
        startFrame = self.ui.STARTIME.text()
        allNucleus = mc.ls(type="nucleus")

        for nucleus in allNucleus:
            mc.setAttr("{}.startFrame".format(nucleus), int(startFrame))

    def CreateNclothCach(self):
        '''
        Create an nCache file.
        '''
        eval('''
        doCreateNclothCache 5 { "2", "1", "10",
        "OneFile", "1", "","1","","0", "add", "0",
        "1", "1","0","1","mcx" }''')

    def openFilewin(self):
        '''
        Choose cache file location
        '''
        filePath = QFileDialog.getExistingDirectory()
        if not filePath == '':
            self.ui.CACHEPATH.setText(filePath)

    def slavePlayblast(self):
        '''
        Choose mov file location
        '''
        formatList = self.ui.selectformat.currentText()
        if formatList == "qt":
            fileFrmat = "mov"
        elif formatList == "avi":
            fileFrmat = "avi"
        elif formatList == "image":
            fileFrmat = self.ui.Encoding.currentText()

        PlayblastName = self.filename[:-3]
        self.workspacePath = mc.workspace(q=True, fullName=True)+"/movies"
        if not os.path.isdir(self.workspacePath):
            os.mkdir(self.workspacePath)
        fileName, filePath = QFileDialog.getSaveFileName(
            None, "PlayblastPath",
            self.workspacePath, "All Files (*.%s)" % fileFrmat)
        if not fileName == '':
            self.ui.playblastPath.setText(fileName)

    def setAlembicFilePath(self):
        '''
        Import ABC file path
        '''
        maFilePath = os.path.split(mc.file(q=True, exn=True))[0]
        filePath = QFileDialog.getOpenFileName(
             caption="select ABC file", dir=maFilePath,
             filter="All Files (*.abc)")[0]
        print(filePath)
        if filePath:
            self.ui.alembicPath.setText(filePath)

    def importBS(self):
        '''
        Click Importing ABC cache and merging BS
        '''
        alembicPath = self.ui.alembicPath.text()
        print(alembicPath)
        reload(alemibcBS)
        alemibcBS.importBS(alembicPath)

    def switchFormat(self):
        '''
        Switch file Encoding
        '''
        formatList = self.ui.selectformat.currentText()
        qt = ["H.264", "DV-PAL", "photo - JPEG", "PNG"]
        avi = ["none"]
        image = ["jpg", "tif", "PNG"]
        self.ui.Encoding.clear()
        if formatList == "qt":
            self.ui.Encoding.addItems(qt)
        elif formatList == "avi":
            self.ui.Encoding.addItems(avi)
        elif formatList == "image":
            self.ui.Encoding.addItems(image)

    def playBlastOptions(self):
        displaySize = self.ui.widthHeight.currentText()
        if displaySize == "4096*1742":
            self.widthHeight = [4096, 1742]
        elif displaySize == "From Render Settings":
            width = maya.cmds.getAttr("defaultResolution.width")
            height = maya.cmds.getAttr("defaultResolution.height")
            self.widthHeight = [width, height]
        elif displaySize == "2048*871":
            self.widthHeight = [2048, 871]
        elif displaySize == "HD_1080":
            self.widthHeight = [1920, 1080]

        blastformat = self.ui.selectformat.currentText()
        compression = self.ui.Encoding.currentText()

        openviewer = True
        savefilename = self.ui.playblastPath.text()

        if self.ui.selectformat.currentText() == "image":
            openviewer = False
            savefilename = os.path.splitext(self.ui.playblastPath.text())[0]
            self.ui.playblastPath.setText(savefilename)
        elif self.ui.selectformat.currentText() == "qt":
            openviewer = True
            savefilename = os.path.splitext(
                self.ui.playblastPath.text())[0]+".mov"
            self.ui.playblastPath.setText(savefilename)
        elif self.ui.selectformat.currentText() == "avi":
            openviewer = True
            savefilename = os.path.splitext(
                self.ui.playblastPath.text())[0]+".avi"
            self.ui.playblastPath.setText(savefilename)

        eval("showHud()")
        mc.playblast(
            percent=100, clearCache=True, fo=True, format=blastformat,
            filename=savefilename, viewer=openviewer,
            showOrnaments=True, compression=compression, quality=100,
            widthHeight=self.widthHeight)

    def copyFile(self):
        '''
        copy the selected CacheFile to the server
        '''
        getpath = self.ui.CACHEPATH.text()
        selCache = mc.ls(sl=1, type="cacheFile")
        for cache in selCache:
            cahcepath = mc.getAttr("{}.cachePath".format(cache))
            cahcename = mc.getAttr("{}.cacheName".format(cache))
            osPath = os.path.join(cahcepath, cahcename)
            cacheXml = os.path.isfile(osPath+".xml")
            cacheMcx = os.path.isfile(osPath+".mcx")
            cacheMc = os.path.isfile(osPath+".mc")
            newPathXml = os.path.join(getpath, cahcename)
            if cacheXml and (cacheMcx or cacheMc):
                if os.path.exists(getpath):
                    shutil.copyfile(osPath+".xml", newPathXml+".xml")
            if cacheMcx:
                shutil.copyfile(osPath+".mcx", newPathXml+".mcx")
            else:
                shutil.copyfile(osPath+".mc", newPathXml+".mc")

    def setCachePath(self):
        '''
        set Cache path
        '''
        getpath = self.ui.CACHEPATH.text()
        selCache = mc.ls(sl=1, type="cacheFile")
        for i in selCache:
            mc.setAttr(
                "%s.cachePath" % i,
                getpath, type="string")

    def listRefRoles(self):
        '''
        list All reference
        '''
        allRef = mc.ls("CH*", type="reference")
        print(allRef)
        if len(allRef) > 1:
            self.ui.RolesCom.clear()
            self.ui.RolesCom.addItem("------------")
            for ch in allRef:
                if "CH" in ch:
                    self.ui.RolesCom.addItem(ch)
        else:
            self.ui.RolesCom.clear()
            self.ui.RolesCom.addItem("------------")
        # self.addItems()

        print(self.ui.TaggingMode.isChecked())

    def sortLayer(self):
        sortlay.SoftDisplayLayer()

    def setUITip(self):
        '''
        pass
        '''
        self.ui.simMethod.setStatusTip("设置毛发解算模式")
        self.ui.USENucleusLAB.setStatusTip("设置毛发是否使用解算器")
        self.ui.RefreshList.setStatusTip("刷新列表")
        self.ui.CLOSESOLVES.setStatusTip("""选择一个解算器关闭其他的解算器
        不选择解算器关闭所以解算器""")
        self.ui.DoNcache.setStatusTip("创建Ncahe")
        self.ui.SETSTTIME.setStatusTip("""设置所有解算器开始帧""")


def showCfxWin():
    if mc.window("hzCFX", ex=True):
        mc.deleteUI("hzCFX")
    run = CfxToolUI(getMainWindowPtr())
