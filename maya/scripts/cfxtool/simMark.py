import maya.cmds as mc


def addMark():
    sel = mc.ls(sl=1)
    for i in sel:
        mc.addAttr(i, ln="cfxAttr", dt="string")


def delMark():
    sel = mc.ls(sl=1)
    for i in sel:
        if mc.attributeQuery("cfxAttr", node=i, ex=True):
            mc.deleteAttr(i, at="cfxAttr")
