import maya.cmds as mc


def showNucleusNrigid(colType):
    '''
    Show collision
    '''
    Sel = mc.ls(selection=True)[0]
    nRigidNode = list(set(mc.listConnections(
        Sel, source=True, destination=True,  type=colType)))
    for item in nRigidNode:
        Shape = mc.listRelatives(item, shapes=True)
        if mc.getAttr("%s.solverDisplay" % item) == 0:
            mc.setAttr("%s.solverDisplay" % item, 1)
        else:
            mc.setAttr("%s.solverDisplay" % item, 0)
