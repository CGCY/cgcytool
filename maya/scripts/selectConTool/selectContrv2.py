import sys
import os
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QWidget, QApplication
from PySide2.QtGui import QIcon, QIntValidator, QFont
import PySide2.QtCore as QtCore
from shiboken2 import wrapInstance
from functools import partial
import maya.OpenMayaUI
import maya.cmds as mc
from selectConTool.core import cgcycore

osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "selectConToolUI.ui")


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class AniToolshowWin(QWidget):

    def __init__(self, getMain, parent=None):
        super(AniToolshowWin, self).__init__(parent)
        self.ui = QUiLoader().load(uipath, getMain)

        self.initUI()

        # button Command
        self.setButtonCommand()

        self.listRoles()

        self.setLineEdit()

        # set window? unclear meaning.But must have
        self.ui.setWindowFlags(QtCore.Qt.Window)

        self.ui.show()

    def setButtonCommand(self):
        '''
        set button click Command
        '''
        self.ui.RefreshButton.clicked.connect(partial(self.listRoles))
        self.ui.selectAllFingerbutton.clicked.connect(
            partial(self.selectFinger))
        self.ui.selectMainbutton.clicked.connect(
            partial(self.selectMain))
        self.ui.select_R.clicked.connect(partial(
            lambda: self.selectAboutFinger("R")))
        self.ui.select_L.clicked.connect(partial(
            lambda: self.selectAboutFinger("L")))
        self.ui.select_ALL.clicked.connect(
            partial(self.selectAllContr))
        self.ui.Bakebutton.clicked.connect(
            partial(self.bakSim))
        self.ui.setCH.clicked.connect(
            partial(self.setCHRe))

        # R
        self.ui.FKWristR.clicked.connect(partial(
            lambda: self.selectSPContr("FKWrist_R")))
        self.ui.ElbowR.clicked.connect(partial(
            lambda: self.selectSPContr("FKElbow_R")))
        self.ui.ShoulderR.clicked.connect(partial(
            lambda: self.selectSPContr("FKShoulder_R")))
        self.ui.ToesR.clicked.connect(partial(
            lambda: self.selectSPContr("FKToes_R")))
        self.ui.AnkleR.clicked.connect(partial(
            lambda: self.selectSPContr("FKAnkle_R")))
        self.ui.KneeR.clicked.connect(partial(
            lambda: self.selectSPContr("FKKnee_R")))
        self.ui.HipR.clicked.connect(partial(
            lambda: self.selectSPContr("FKHip_R")))
        self.ui.ScapulaR.clicked.connect(partial(
            lambda: self.selectSPContr("FKScapula_R")))

        self.ui.IKPoleR.clicked.connect(partial(
            lambda: self.selectSPContr("PoleArm_R")))
        self.ui.RIK.clicked.connect(partial(
            lambda: self.selectSPContr("IKArm_R")))

        self.ui.fingersRIKFK.clicked.connect(partial(
            lambda: self.selectSPContr("FKIKArm_R")))

        self.ui.fingersR.clicked.connect(partial(
            lambda: self.selectSPContr("Fingers_R")))
        self.ui.AllFingerR.clicked.connect(partial(
            lambda: self.selectAboutFinger("R")))

        self.ui.LegRIKFK.clicked.connect(partial(
            lambda: self.selectSPContr("FKIKLeg_R")))
        self.ui.IKLegPoleR.clicked.connect(partial(
            lambda: self.selectSPContr("PoleLeg_R")))
        self.ui.legRIK.clicked.connect(partial(
            lambda: self.selectSPContr("IKLeg_R")))

        self.ui.RollToesEnd_R.clicked.connect(partial(
            lambda: self.selectSPContr("RollToesEnd_R")))
        self.ui.RollToes_R.clicked.connect(partial(
            lambda: self.selectSPContr("RollToes_R")))
        self.ui.RollHeel_R.clicked.connect(partial(
            lambda: self.selectSPContr("RollHeel_R")))

        self.ui.PinkyFinger3R.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger3_R")))
        self.ui.PinkyFinger2R.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger2_R")))
        self.ui.PinkyFinger1R.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger1_R")))

        self.ui.FKRingFinger3R.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger3_R")))
        self.ui.FKRingFinger2R.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger2_R")))
        self.ui.FKRingFinger1R.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger1_R")))

        self.ui.MiddleFinger3R.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger3_R")))
        self.ui.MiddleFinger2R.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger2_R")))
        self.ui.MiddleFinger1R.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger1_R")))

        self.ui.IndexFinger3R.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger3_R")))
        self.ui.IndexFinger2R.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger2_R")))
        self.ui.IndexFinger1R.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger1_R")))

        self.ui.ThumbFinger3R.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger3_R")))
        self.ui.ThumbFinger2R.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger2_R")))
        self.ui.ThumbFinger1R.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger1_R")))

        self.ui.WristR.clicked.connect(partial(
            lambda: self.selectSPContr("FKWrist_R")))

        # L
        self.ui.FKWristL.clicked.connect(partial(
            lambda: self.selectSPContr("FKWrist_L")))
        self.ui.ElbowL.clicked.connect(partial(
            lambda: self.selectSPContr("FKElbow_L")))
        self.ui.ShoulderL.clicked.connect(partial(
            lambda: self.selectSPContr("FKShoulder_L")))
        self.ui.ToesL.clicked.connect(partial(
            lambda: self.selectSPContr("FKToes_L")))
        self.ui.AnkleL.clicked.connect(partial(
            lambda: self.selectSPContr("FKAnkle_L")))
        self.ui.KneeL.clicked.connect(partial(
            lambda: self.selectSPContr("FKKnee_L")))
        self.ui.HipL.clicked.connect(partial(
            lambda: self.selectSPContr("FKHip_L")))
        self.ui.ScapulaL.clicked.connect(partial(
            lambda: self.selectSPContr("FKScapula_L")))

        self.ui.IKPoleL.clicked.connect(partial(
            lambda: self.selectSPContr("PoleArm_L")))
        self.ui.LIK.clicked.connect(partial(
            lambda: self.selectSPContr("IKArm_L")))

        self.ui.fingersLIKFK.clicked.connect(partial(
            lambda: self.selectSPContr("FKIKArm_L")))

        self.ui.fingersL.clicked.connect(partial(
            lambda: self.selectSPContr("Fingers_L")))
        self.ui.AllFingerL.clicked.connect(partial(
            lambda: self.selectAboutFinger("L")))

        self.ui.LegLIKFK.clicked.connect(partial(
            lambda: self.selectSPContr("FKIKLeg_L")))
        self.ui.IKLegPoleL.clicked.connect(partial(
            lambda: self.selectSPContr("PoleLeg_L")))
        self.ui.legLIK.clicked.connect(partial(
            lambda: self.selectSPContr("IKLeg_L")))

        self.ui.RollToesEnd_L.clicked.connect(partial(
            lambda: self.selectSPContr("RollToesEnd_L")))
        self.ui.RollToes_L.clicked.connect(partial(
            lambda: self.selectSPContr("RollToes_L")))
        self.ui.RollHeel_L.clicked.connect(partial(
            lambda: self.selectSPContr("RollHeel_L")))

        self.ui.PinkyFinger3L.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger3_L")))
        self.ui.PinkyFinger2L.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger2_L")))
        self.ui.PinkyFinger1L.clicked.connect(partial(
            lambda: self.selectSPContr("FKPinkyFinger1_L")))

        self.ui.FKRingFinger3L.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger3_L")))
        self.ui.FKRingFinger2L.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger2_L")))
        self.ui.FKRingFinger1L.clicked.connect(partial(
            lambda: self.selectSPContr("FKRingFinger1_L")))

        self.ui.MiddleFinger3L.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger3_L")))
        self.ui.MiddleFinger2L.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger2_L")))
        self.ui.MiddleFinger1L.clicked.connect(partial(
            lambda: self.selectSPContr("FKMiddleFinger1_L")))

        self.ui.IndexFinger3L.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger3_L")))
        self.ui.IndexFinger2L.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger2_L")))
        self.ui.IndexFinger1L.clicked.connect(partial(
            lambda: self.selectSPContr("FKIndexFinger1_L")))

        self.ui.ThumbFinger3L.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger3_L")))
        self.ui.ThumbFinger2L.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger2_L")))
        self.ui.ThumbFinger1L.clicked.connect(partial(
            lambda: self.selectSPContr("FKThumbFinger1_L")))

        self.ui.WristL.clicked.connect(partial(
            lambda: self.selectSPContr("FKWrist_L")))

        # M
        self.ui.HeadM.clicked.connect(partial(
            lambda: self.selectSPContr("FKHead_M")))
        self.ui.Swinger.clicked.connect(partial(
            lambda: self.selectSPContr("HipSwinger_M")))
        self.ui.NeckM.clicked.connect(partial(
            lambda: self.selectSPContr("FKNeck_M")))
        self.ui.Main.clicked.connect(partial(
            lambda: self.selectSPContr("Main")))

        self.ui.ChestM.clicked.connect(partial(
            lambda: self.selectSPContr("FKChest_M")))
        self.ui.Spine1M.clicked.connect(partial(
            lambda: self.selectSPContr("FKSpine1_M")))
        self.ui.RootM.clicked.connect(partial(
            lambda: self.selectSPContr("FKRoot_M")))
        self.ui.RootXM.clicked.connect(partial(
            lambda: self.selectSPContr("RootX_M")))
        self.ui.ALL.clicked.connect(
            partial(self.selectAllContr))

    def setCHRe(self):
        """
        Select references by selected roles
        """
        self.getCom()
        sel = mc.ls(sl=True)[0]
        name = sel.split(":")[0]
        self.ui.AllcomboBox.setCurrentText(name+"RN")

    def listRoles(self):
        """
        Refresh the list of referenced roles
        """
        allCH = cgcycore.listRef()
        if len(allCH) > 1:
            self.ui.AllcomboBox.clear()
            for ch in allCH:
                if "CH" in ch:
                    self.ui.AllcomboBox.addItem(ch)

    def getCom(self):
        """
        Get dropdown menu selection
        """
        self.chName = self.ui.AllcomboBox.currentText()

    def selectFinger(self):
        '''
        select all Finger
        '''
        self.getCom()

        if not self.chName == "-":
            fkList = mc.ls("{0}:*FK*Finger*Shape".format(
                self.chName.rsplit("R", 1)[0]))

            CHFinger = list()
            for con in fkList:
                conParent = (mc.listRelatives(con, p=True))
                CHFinger.append(conParent[0])
            mc.select(CHFinger)

    def selectAboutFinger(self, about):
        '''
        select about Finger
        '''
        self.getCom()

        if not self.chName == "-":
            fkList = mc.ls("{0}:*FK*Finger*_{1}Shape".format(
                self.chName.rsplit("R", 1)[0], about))

            CHFinger = list()
            for con in fkList:
                conParent = (mc.listRelatives(con, p=True))
                CHFinger.append(conParent[0])
            mc.select(CHFinger)

    def selectMain(self):
        """
        select role Main
        """
        self.getCom()
        Main = mc.ls("{0}:Main".format(
            self.chName.rsplit("R", 1)[0]))
        mc.select(cl=True)
        mc.select(Main)

    def selectAllContr(self):
        """
        select role Main
        """

        self.getCom()

        listContr = mc.ls(
            "%s:*_*" % self.chName.rsplit("R", 1)[0], type="nurbsCurve")
        CHFinger = list()
        for con in listContr:
            conParent = (mc.listRelatives(con, p=True))
            CHFinger.append(conParent[0])
        mc.select(CHFinger)

    def selectSPContr(self, name):
        '''
        Select a specific controller
        '''
        self.getCom()
        mc.select("%s:%s" % (self.chName.rsplit("R", 1)[0], name))

    def bakSim(self):
        """
        Bakes the keyframes of the selected object
        """
        mintime = self.ui.StartEdit.text()
        maxtime = self.ui.EndEdit.text()
        mc.bakeResults(t=(mintime, maxtime), simulation=True)

    def initUI(self):
        '''
        set UI color and Icon
        '''
        self.ui.bordergroup.setStyleSheet(
            'background-color: #ffffee; color: #ffffff;')
        self.ui.Main.setStyleSheet(
            'background-color: #ff7800;')
        self.ui.ALL.setStyleSheet(
            'background-color: #fd4700;')
        self.ui.RIK.setStyleSheet(
            'background-color: #ff0000;')
        self.ui.LIK.setStyleSheet(
            'background-color: #ff0072;')
        self.ui.legRIK.setStyleSheet(
            'background-color: #ff0000;')
        self.ui.legLIK.setStyleSheet(
            'background-color: #ff0072;')
        self.ui.AllFingerR.setStyleSheet(
            'background-color: #5ba3c9;')
        self.ui.AllFingerL.setStyleSheet(
            'background-color: #5dc7aa;')
        self.ui.fingersR.setStyleSheet(
            'background-color: #5db9c7;')
        self.ui.fingersL.setStyleSheet(
            'background-color: #5dc7c3;')

        bordericon = os.path.join(osfile, "icon/men.jpg")
        bordericon = bordericon.replace("\\", "/")
        self.ui.border.setStyleSheet(
            "border-image: url('{0}')".format(bordericon))

        ikPoleRicon = os.path.join(osfile, "icon/IKFK.png")
        ikPoleRicon = ikPoleRicon.replace("\\", "/")
        self.ui.IKPoleR.setStyleSheet(
            "background-image: url('{0}')".format(ikPoleRicon))
        self.ui.IKPoleL.setStyleSheet(
            "background-image: url('{0}')".format(ikPoleRicon))

        legikPoleRicon = os.path.join(osfile, "icon/LegIKFK.png")
        legikPoleRicon = legikPoleRicon.replace("\\", "/")
        self.ui.IKLegPoleR.setStyleSheet(
            "background-image: url('{0}')".format(legikPoleRicon))
        self.ui.IKLegPoleL.setStyleSheet(
            "background-image: url('{0}')".format(legikPoleRicon))

        rollicon = os.path.join(osfile, "icon/Roll.png")
        rollicon = rollicon.replace("\\", "/")
        self.ui.RollToesEnd_R.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))
        self.ui.RollToes_R.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))
        self.ui.RollHeel_R.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))
        self.ui.RollHeel_L.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))
        self.ui.RollToes_L.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))
        self.ui.RollToesEnd_L.setStyleSheet(
            "background-image: url('{0}')".format(rollicon))

        ikfkswitchicon = os.path.join(osfile, "icon/ikfkswitch.png")
        ikfkswitchicon = ikfkswitchicon.replace("\\", "/")
        self.ui.fingersRIKFK.setStyleSheet(
            "background-image: url('{0}')".format(ikfkswitchicon))
        self.ui.fingersLIKFK.setStyleSheet(
            "background-image: url('{0}')".format(ikfkswitchicon))
        self.ui.LegRIKFK.setStyleSheet(
            "background-image: url('{0}')".format(ikfkswitchicon))
        self.ui.LegLIKFK.setStyleSheet(
            "background-image: url('{0}')".format(ikfkswitchicon))

        swingericon = os.path.join(osfile, "icon/Swinger.png")
        swingericon = swingericon.replace("\\", "/")
        self.ui.Swinger.setStyleSheet(
            "background-image: url('{0}')".format(swingericon))

        btn1icon = QIcon(os.path.join(osfile, "icon/btn1Icon.png"))
        self.ui.RefreshButton.setFixedWidth(20)
        self.ui.RefreshButton.setFixedHeight(20)
        self.ui.RefreshButton.setIcon(btn1icon)

        self.ui.setCH.setFixedWidth(40)
        self.ui.setCH.setFixedHeight(20)

        btn2icon = QIcon(os.path.join(osfile, "icon/help.png"))
        self.ui.helpButon.setFixedWidth(20)
        self.ui.helpButon.setFixedHeight(20)
        self.ui.helpButon.setIcon(btn2icon)

        btn2icon = QIcon(os.path.join(osfile, "icon/windowIcon.png"))
        self.ui.setWindowIcon(btn2icon)

    def setLineEdit(self):
        """
        Get scene time slider and update UI information
        """
        maxtime = int(mc.playbackOptions(q=True, max=True))
        mintime = int(mc.playbackOptions(q=True, min=True))

        # Limit the text box to the left
        self.ui.StartEdit.setAlignment(QtCore.Qt.AlignRight)
        self.ui.EndEdit.setAlignment(QtCore.Qt.AlignRight)

        # Limit text boxes to integers
        self.ui.StartEdit.setValidator(QIntValidator())
        self.ui.EndEdit.setValidator(QIntValidator())

        # Limit text boxes size
        self.ui.StartEdit.setFixedWidth(50)
        self.ui.EndEdit.setFixedWidth(50)

        self.ui.StartEdit.setText(str(mintime))
        self.ui.EndEdit.setText(str(maxtime))


def showSelectToolWin():
    if mc.window("selectCon", ex=True):
        mc.deleteUI("selectCon")
    run = AniToolshowWin(getMainWindowPtr())
