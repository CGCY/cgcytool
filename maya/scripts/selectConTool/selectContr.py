#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import os
from PySide2.QtWidgets import (
    QWidget, QPushButton, QComboBox, QHBoxLayout, QLabel, QVBoxLayout,
    QLineEdit, QSplitter, QStyleFactory, QMessageBox)
import PySide2.QtCore as QtCore
from PySide2.QtGui import QIcon, QIntValidator, QPixmap
import maya.cmds as mc
import maya.OpenMayaUI
from shiboken2 import wrapInstance
from selectConTool.core import cgcycore


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class AniToolshowWin(QWidget):

    def __init__(self, parent=None):
        super(AniToolshowWin, self).__init__(parent)

        # Initialize UI
        self.initUI()

        # set button command
        self.command()
        self.setWindowFlags(QtCore.Qt.Window)

    def initUI(self):

        osfile = os.path.split(__file__)[0]
        btn1iconPath = os.path.join(osfile, "icon/btn1Icon.png")
        btn1icon = QIcon(btn1iconPath)

        self.QCom1 = QComboBox()

        self.QCom1.addItem("-")

        self.QCom2 = QComboBox()
        self.QCom2.addItems(["*", "L", "R"])

        self.btn1 = QPushButton()

        # Limit button size
        self.btn1.setFixedWidth(20)
        self.btn1.setFixedHeight(20)
        self.btn1.setIcon(btn1icon)

        #
        self.btn2 = QPushButton("selectFinger")
        self.btn3 = QPushButton("selectMain")
        self.btn5 = QPushButton("selectAllContr")
        #

        btn6iconPath = os.path.join(osfile, "icon/help.png")
        btn6icon = QIcon(btn6iconPath)

        self.btn6 = QPushButton()
        self.btn6.setFixedWidth(20)
        self.btn6.setFixedHeight(20)
        self.btn6.setIcon(btn6icon)

        #
        qlayR = QVBoxLayout()
        qlay1 = QHBoxLayout()
        qlay1.addWidget(self.QCom1)
        qlay1.addWidget(self.QCom2)
        qlay1.addWidget(self.btn1)
        qlay1.addWidget(self.btn2)
        qlay1.addWidget(self.btn3)
        qlay1.addWidget(self.btn5)
        qlay1.addWidget(self.btn6)

        #

        self.labe1 = QLabel("Bake Sim:")
        self.btn4 = QPushButton("Bake")
        self.lineEit1 = QLineEdit()

        # Set the spring
        self.Spacer1 = QSplitter(QtCore.Qt.Horizontal)
        self.lineEit2 = QLineEdit()

        # Limit the text box to the left
        self.lineEit1.setAlignment(QtCore.Qt.AlignRight)
        self.lineEit2.setAlignment(QtCore.Qt.AlignRight)

        # Limit text boxes to integers
        self.lineEit1.setValidator(QIntValidator())
        self.lineEit2.setValidator(QIntValidator())

        # Limit text boxes size
        self.lineEit1.setFixedWidth(50)
        self.lineEit2.setFixedWidth(50)
        self.setLineEdit()
        #

        #
        qlay2 = QHBoxLayout()
        qlay2.addWidget(self.labe1)
        qlay2.addWidget(self.Spacer1)
        qlay2.addWidget(self.lineEit1)
        qlay2.addWidget(self.Spacer1)
        qlay2.addWidget(self.lineEit2)
        qlay2.addWidget(self.btn4)
        #

        qlayR.addLayout(qlay1)
        qlayR.addLayout(qlay2)

        self.setLayout(qlayR)

        # set window Icon
        iconPath = os.path.join(osfile, "icon/windowIcon.png")
        appicon = QIcon(iconPath)

        self.setWindowIcon(appicon)
        #

        self.setWindowTitle("selectCon")
        self.setGeometry(1500, 500, 800, 100)
        self.setObjectName("selectCon")

    def listRoles(self):
        """
        Refresh the list of referenced roles
        """
        allCH = cgcycore.listRef()
        if len(allCH) > 1:
            self.QCom1.clear()
            for ch in allCH:
                if "CH" in ch:
                    self.QCom1.addItem(ch)

    def command(self):
        # btn1 Refresh role list
        self.btn1.clicked.connect(self.listRoles)

        # btn2 select role Finger
        self.btn2.clicked.connect(self.selectFinger)

        # btn3 select role Main
        self.btn3.clicked.connect(self.selectMain)

        # btn4 Bake keys
        self.btn4.clicked.connect(self.bakSim)

        # btn5 select role AllContr
        self.btn5.clicked.connect(self.selectAllContr)

        # btn6 show help
        self.btn6.clicked.connect(self.helpDoc)

    def getCom(self):
        """
        Get dropdown menu selection
        """
        self.chName = self.QCom1.currentText()
        self.about = self.QCom2.currentText()

    def selectFinger(self):
        '''
        select role Finger
        '''
        self.getCom()

        if not self.chName == "-":
            fkList = mc.ls("{0}:*FK*Finger*_{1}Shape".format(
                self.chName[:-2], self.about))

            mc.select(cl=True)
            mc.select(fkList)
            mc.pickWalk(d="up")

    def selectMain(self):
        """
        select role Main
        """
        self.getCom()
        Main = mc.ls("{0}:Main".format(
            self.chName[:-2]))
        mc.select(cl=True)
        mc.select(Main)

    def setLineEdit(self):
        """
        Get scene time slider and update UI information
        """
        maxtime = int(mc.playbackOptions(q=True, max=True))
        mintime = int(mc.playbackOptions(q=True, min=True))
        self.lineEit1.setText(str(mintime))
        self.lineEit2.setText(str(maxtime))

    def bakSim(self):
        """
        Bakes the keyframes of the selected object
        """
        mintime = self.lineEit1.text()
        maxtime = self.lineEit2.text()
        mc.bakeResults(t=(mintime, maxtime), simulation=True)

    def selectAllContr(self):
        """
        select role Main
        """

        self.getCom()

        listContr = mc.ls("%s:*_*" % self.chName[:-2], type="nurbsCurve")
        selList = [i.split("Shape")[0] for i in listContr]

        mc.select(selList)

    def helpDoc(self):
        text = '''
        点击刷新图标获取场景中的角色
        selectFinger    选择当前角色手指的控制器
        selectMain   选择角色大环
        selectAllContr  选择当前角色所有的控制器
        '''
        Title = "help"
        QMessageBox.question(self, Title, text)


if mc.window("selectCon", ex=True):
    mc.deleteUI("selectCon")
run = AniToolshowWin(getMainWindowPtr())
run.show()
