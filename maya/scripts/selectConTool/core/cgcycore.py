import maya.cmds as mc


def listRef():
    '''
    list All reference
    '''
    allRef = mc.ls(type="reference")
    return allRef
