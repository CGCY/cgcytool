#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
from PySide2.QtGui import QIntValidator
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QWidget, QFileDialog, QLineEdit
import PySide2.QtCore as QtCore
from functools import partial
import maya.OpenMayaUI
from shiboken2 import wrapInstance
import maya.cmds as mc
from maya.mel import eval
from qdarkstyle import style
from faceImage import importNode
reload(importNode)

osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "importimage.ui")


class QLineEditPath(QLineEdit):

    def __init__(self, parent=None):
        super(QLineEditPath, self).__init__(parent)

    def dragEnterEvent(self, event):

        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):

        if event.mimeData().hasUrls():
            url = event.mimeData().urls()[0]
            self.setText(url.toLocalFile())


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class ImportImagesWin(QWidget):
    def __init__(self, getMain, parent=None):
        super(ImportImagesWin, self).__init__(parent)
        # load UI File
        self.ui = QUiLoader().load(uipath, getMain)
        self.initUI()

        self.ui.setWindowFlags(QtCore.Qt.Window)

        self.ui.show()

    def initUI(self):
        self.ui.spaceSize.setValidator(QIntValidator())

        self.ui.imageFile.clicked.connect(partial(self.openimageFile))
        self.ui.Audiofile.clicked.connect(partial(self.openAudioFile))
        self.ui.importNode.clicked.connect(partial(self.importimageNode))
        self.ui.UPoffset.clicked.connect(partial(self.updeteOffset))

        self.ui.offsetSlider.valueChanged.connect(self.moveSlider)
        self.ui.offsetSlider.sliderReleased.connect(self.releasedSlider)

        self.ui.setStyleSheet(style.qss)

        self.qudiolin1 = QLineEditPath()
        self.ui.AudiLay.addWidget(self.qudiolin1)

        self.imagelin1 = QLineEditPath()
        self.ui.imageLay.addWidget(self.imagelin1)

        self.ui.offsetNum.returnPressed.connect(partial(self.setoffset))
        self.ui.offsetNum.setValidator(QIntValidator())

    def openimageFile(self):
        filepath = QFileDialog.getOpenFileName(
                    caption="select ABC file", dir="",
                    filter="All Files (*.jpg *.mov *.avi *.mp4)")[0]
        if not filepath == "":
            print(filepath)
            self.imagelin1.setText(str(filepath))

    def openAudioFile(self):
        filepath = QFileDialog.getOpenFileName(
                    caption="select ABC file", dir="",
                    filter="All Files (*.wav)")[0]
        if not filepath == "":
            print(filepath)
            self.qudiolin1.setText(str(filepath))

    def importimageNode(self):
        imagePath = self.imagelin1.text()
        spaceSize = int(self.ui.spaceSize.text())
        if not self.qudiolin1.text() == "":
            audiopath = importNode.copyAudio(self.qudiolin1.text())
            self.qudiolin1.setText(str(audiopath))
            sound = importNode.importAudio(self.qudiolin1.text())

        if imagePath.split(".")[-1] == "jpg":
            # print(imagePath.split(".")[-1])
            importNode.importImages(20, 0, imagePath, spaceSize)
        else:
            importNode.importImages(0, 2, imagePath, spaceSize)

    def setoffset(self):
        sel = mc.ls(sl=True)
        imageplane = mc.listRelatives(sel, type="imagePlane", shapes=True)
        if imageplane:
            imageplaneoffset = mc.setAttr(
                "%s.frameOffset" % imageplane[0],
                int(self.ui.offsetNum.text()))

    def moveSlider(self):
        self.ui.offsetNum.setText(
            str(int(self.ui.offsetNum.text())+self.ui.offsetSlider.value()))
        # print(self.ui.offsetSlider.value())
        sel = mc.ls(sl=True)
        imageplane = mc.listRelatives(sel, type="imagePlane", shapes=True)
        if imageplane:
            mc.setAttr(
                "%s.frameOffset" % imageplane[0],
                int(self.ui.offsetNum.text()))

    def releasedSlider(self):
        self.ui.offsetSlider.setValue(0)

    def updeteOffset(self):
        sel = mc.ls(sl=True)
        imageplane = mc.listRelatives(sel, type="imagePlane", shapes=True)
        if imageplane:
            imageplaneoffset = mc.getAttr(
                "%s.frameOffset" % imageplane[0])
            self.ui.offsetNum.setText(str(imageplaneoffset))


def showFaceWin():
    if mc.window("faceImage", ex=True):
        mc.deleteUI("faceImage")
    run = ImportImagesWin(getMainWindowPtr())
