#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
import shutil
import maya.cmds as mc
from time import sleep
from maya.mel import eval


def importImages(offset, fileType, imagePath, spaceSize=3):
    '''
    importImages(20, 0, "D:/image.0101.jpg", 3)
    fileType = 0  导入图片
    fileType = 2  导入视频
    '''

    sel = mc.ls(sl=True)[:2]
    if len(sel) == 2:
        myImagePlane = mc.imagePlane()
        mc.setAttr("%s.type" % myImagePlane[1], fileType)
        mc.setAttr(
            "%s.imageName" % myImagePlane[0],
            imagePath,
            type="string")
        imageSize = mc.imagePlane(myImagePlane[1], q=True, iz=True)
        mc.setAttr("%s.width" % myImagePlane[1], imageSize[0]*0.01)
        mc.setAttr("%s.height" % myImagePlane[1], imageSize[1]*0.01)
        mc.setAttr("%s.useFrameExtension" % myImagePlane[1], 1)

        loc, hand = mc.duplicate(sel, rr=True)

        mc.parent(loc, hand, w=True)
        handxfrom = mc.xform(hand, sp=True, q=True, ws=True)
        handRo = mc.getAttr("%s.rotateZ" % sel[1])
        grp = mc.group(loc)

        mc.setAttr(
            "%s.scalePivot" % grp,
            handxfrom[0],
            handxfrom[1],
            handxfrom[2])

        mc.setAttr(
            "%s.rotatePivot" % grp,
            handxfrom[0],
            handxfrom[1],
            handxfrom[2])

        mc.setAttr(
            "%s.rotateY" % grp, -180)

        mc.parent(loc, hand, w=True)

        ctrlBoxtr = mc.xform(loc, sp=True, q=True, ws=True)
        ctrlBoxro = mc.xform(loc, ro=True, q=True, ws=True)
        mc.delete(loc, hand, grp)

        mc.setAttr(
            "%s.translate" % myImagePlane[0],
            ctrlBoxtr[0],
            ctrlBoxtr[1],
            ctrlBoxtr[2])

        mc.setAttr(
            "%s.rotate" % myImagePlane[0],
            ctrlBoxro[0],
            ctrlBoxro[1],
            ctrlBoxro[2])

        mc.setAttr(
            "%s.scale" % myImagePlane[0], -spaceSize, spaceSize, spaceSize)

        mc.setAttr("%s.frameOffset" % myImagePlane[0], offset)

        conGrp = mc.group(
            myImagePlane[0], n="%simageGrp" % sel[0].split("_")[1])
        mc.parentConstraint(sel[0], conGrp, mo=True, weight=1)
        mc.select(myImagePlane[1])
        mc.ShowAttributeEditorOrChannelBox()
        mc.ShowAttributeEditorOrChannelBox()


def importAudio(audioPath):
    '''
    importAudio("D:/EP010_SC007_CAM136.wav")
    '''
    audiofile = os.path.split(audioPath)[1]
    audioNodeName = os.path.splitext(audiofile)[0]
    print(audioPath, audiofile, audioNodeName)
    sound = mc.sound(n="shound%s" % audioNodeName, file=audioPath)
    eval("setSoundDisplay %s 1;" % sound)
    return sound


def copyAudio(path):
    '''
    copyAudio(path)
    input AudioFile path
    '''
    audiopath, audioFile = os.path.split(path)
    try:
        int(audioFile[0])
        shutil.copyfile(path, audiopath+"/audiopath"+audioFile)
        return audiopath+"/audiopath"+audioFile
    except BaseException:
        print("Truue")
        return path
