#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json
import maya.cmds as mc
from maya.mel import eval


def exportJson(jsonPath):
    shaderDict = {}
    cout = []
    sel = mc.ls(sl=True)
    sel = [mc.listRelatives(item, shapes=True)[0] for item in sel]
    if not sel:
        sel = mc.ls(type="mesh")
    for s in sel:
        try:
            shadingGrps = mc.listConnections(s, type='shadingEngine')
            shaders = mc.ls(mc.listConnections(shadingGrps), materials=1)
            shaders = list(set(shaders))
            print(shaders)
            for shader in shaders:
                if not mc.nodeType(shader) == "displacementShader":
                    mc.select(shader, r=True)
                    mc.hyperShade(objects="")
                    sur = mc.ls(sl=True)
                    mc.select(cl=True)
                    for s in sur:
                        shaderDict[s] = shader
        except BaseException:
            cout.append(s)
    with open(jsonPath, "w") as f:
        json.dump(shaderDict, f, indent=4)

    # print(shaderDict)
    if len(cout) < 0:
        eval('print "%s model is not exported  %s"' % (len(cout), cout))
    else:
        eval('print "Export %s models, file  %s"' % (len(sel), jsonPath))


def importJson(jsonPath):
    cout = []
    with open(jsonPath, "r") as f:
        dataload = json.load(f)
    # print(dataload)
    for key, value in dataload.items():
        try:
            keyobject = key
            if len(keyobject.split(".f[", 1)) > 1:
                keyobject = mc.listRelatives(keyobject.split(
                    ".f[", 1)[0], shapes=True)[0]
            shadingGrps = mc.listConnections(keyobject, type='shadingEngine')
            if shadingGrps:
                shaders = mc.ls(mc.listConnections(shadingGrps), materials=1)
                shaders = list(set(shaders))
                for shader in shaders:
                    if not mc.nodeType(shader) == "displacementShader":
                        mc.select(shader, r=True)
                        mc.hyperShade(objects="")
                        sur = mc.ls(sl=True)
                        if key in sur and not shader == value:
                            mc.rename(shader, value)
        except BaseException:
            print("cuo")
            cout.insert(0, key)
    if len(cout) > 0:
        eval('print "%s models not found %s"' % (len(cout), cout))
    else:
        eval('print "Successfully imported %s model file %s"' % (
            len(dataload),
            jsonPath))
