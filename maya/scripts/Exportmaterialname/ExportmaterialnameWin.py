#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import (QWidget, QFileDialog)
import PySide2.QtCore as QtCore
import maya.OpenMayaUI
from shiboken2 import wrapInstance
import maya.cmds as mc
from functools import partial
from qdarkstyle import style
from Exportmaterialname import Exportname
reload(Exportname)


osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "untitled.ui")


def getMainWindowPtr():
    mayaMainWindowPtr = maya.OpenMayaUI.MQtUtil.mainWindow()
    mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget)
    return(mayaMainWindow)


class ExportmaterialnameUI(QWidget):

    def __init__(self, getMain, parent=None):
        super(ExportmaterialnameUI, self).__init__(parent)
        # load UI File
        self.ui = QUiLoader().load(uipath, getMain)
        self.initUI()

        self.ui.setWindowFlags(QtCore.Qt.Window)
        self.ui.show()

    def initUI(self):
        self.ui.setStyleSheet(style.qss)
        self.ui.ExportFIle.clicked.connect(partial(self.slaveFile))
        self.ui.importFIle.clicked.connect(partial(self.importFIle))

    def slaveFile(self):
        fileName, filePath = QFileDialog.getSaveFileName(
            None, "导出选项",
            "D:/", "All Files (*.json)")
        print(fileName)
        if fileName:
            Exportname.exportJson(fileName)

    def importFIle(self):
        fileName = QFileDialog.getOpenFileName(
            caption="导入材质",
            directory="D:/",
            filter="All Files (*.json)")
        print(fileName[0])
        if fileName[0]:
            Exportname.importJson(fileName[0])


def showExportmaterialnameUI():
    if mc.window("Exportmaterialname", ex=True):
        mc.deleteUI("Exportmaterialname")
    run = ExportmaterialnameUI(getMainWindowPtr())
