import os
import sys
import qdarkstyle
import time
from datetime import datetime
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import (
    QWidget, QApplication, QFileDialog, QMessageBox, QGraphicsOpacityEffect,
    QMenu, QAction)
from PyQt5.QtGui import (
    QIcon, QPainterPath, QPainter, QBrush, QColor, QPalette, QPixmap)
from PyQt5.QtCore import Qt, QRectF

print("hello")


def bathfile(jobtyoe, mayafile, old, new):

    if not os.path.isdir("C:/mayabatchrename"):

        os.mkdir("C:/mayabatchrename")

    print("gege", mayafile)

    with open("C:/mayabatchrename/mayabatchcachepath.txt", "w") as f:
        f.write(",".join(mayafile))
        f.close()

    with open("scripts/basemel.txt", "r") as f:
        script = f.read()

    script = script.format(jobtyoe, old, new)
    # print("dd", script)

    with open("C:/mayabatchrename/mayabatch.mel", "w") as f:
        f.write(script)
        f.close()

    with open("scripts/mayabat.txt", "r") as f:
        bat = f.read()

    bat = bat.format("C:/mayabatchrename/mayabatch.mel")

    with open("C:/mayabatchrename/mayabat.bat", "w") as f:
        f.write(bat)
        f.close()
    print(os.path.split(sys.argv[0])[0])
    os.system("C:/mayabatchrename/mayabat.bat")


class BatchModificationPathWin(QWidget):
    def __init__(self, parent=None):
        super(BatchModificationPathWin, self).__init__(parent)
        # load UI File
        loadUi("BatchModificationPathUI.ui", self)

        self.initUI()
        # self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        self.setWindowOpacity(0.9)

        self.setWindowIcon(QIcon("windowicon.png"))
        self.show()

    def initUI(self):
        # op = QGraphicsOpacityEffect()
        # op.setOpacity(0.5)
        # self.mafilelist.setGraphicsEffect(op)
        palette = QPalette()
        palette.setBrush(
            QPalette.Background, QBrush(QPixmap("icon/beijing.png")))
        self.setPalette(palette)
        self.rebutton.clicked.connect(
            lambda: self.bathmayafile("batchreferencePathreplace"))
        self.Mapbutton.clicked.connect(
            lambda: self.bathmayafile("batchreplaceMapPath"))
        self.additems.clicked.connect(self.additem)
        self.removeitem.clicked.connect(self.delItem)
        self.minusButton.clicked.connect(self.minimum)
        self.minusButton.setIcon(QIcon("icon/minus.png"))
        self.close_btn.clicked.connect(self.close)
        self.close_btn.setIcon(QIcon("icon/close.png"))

    def bathmayafile(self, jobtyoe):
        before = time.time()
        malist = list()
        for item in self.mafilelist.selectedItems():
            # print(item.text())
            malist.append(item.text())
        old = self.oldName.text()
        new = self.newName.text()
        print(malist, old, new)
        bathfile(jobtyoe, malist, old, new)
        after = time.time()
        nowtime = abs(before-after)
        QMessageBox.question(
            self, "", "路径替换完成,用时%.03s秒" % nowtime, QMessageBox.Yes)

    def additem(self):
        # self.mafilelist.addItem("a")
        print("hello")
        filelist = QFileDialog.getOpenFileNames(
            caption="select Maya File",
            directory="D:/",
            filter="All Files (*.ma *.mb)")
        self.mafilelist.addItems(filelist[0])

    def delItem(self):
        for item in self.mafilelist.selectedItems():
            listoption = self.mafilelist.row(item)
            self.mafilelist.takeItem(listoption)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_position = event.globalPos() - self.frameGeometry(
            ).topLeft()
            event.accept()

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.move(event.globalPos() - self.drag_position)
            event.accept()

    def minimum(self):
        self.showMinimized()

    def contextMenuEvent(self, evt):
        menu = QMenu(self)

        helpMenu = QAction("help", menu)
        helpMenu.triggered.connect(self.helpDoc)

        menu.addAction(helpMenu)

        menu.exec_(evt.globalPos())

    def helpDoc(self):
        os.system(
            "N:/FZWY-S1_E01/Cfx/JueSeMoXing/cgcy-doc/docs/build/html/preface.html")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    run = BatchModificationPathWin()
    sys.exit(app.exec_())
