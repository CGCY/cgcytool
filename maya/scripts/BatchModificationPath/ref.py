import maya.cmds as mc
import time
import os


def referencePathreplace(oldpath, newpath):
    rflist = mc.ls(rf=True)
    for i in rflist:
        ref = mc.referenceQuery(i, f=True)
        print(os.path.isfile(ref))
        pathNewRef = ref.replace(oldpath, newpath, 1)
        print(pathNewRef)
        if not ref == pathNewRef:
            pathNewRef.replace('.ma', '.gpu')
            mc.file(pathNewRef, loadReference=i)
            # mc.file(save=True)


def batchreferencePathreplace(filePath, oldpath, newpath):
    for f in filePath:
        mc.file(
            f,
            open=True,
            force=True,
            loadReferenceDepth="none")
        referencePathreplace(oldpath, newpath)
        # referencePathreplace("N:/", "//king1/G/")
        mc.file(save=True)


def batchreplaceMapPath(filePath, oldpath, newpath):
    for f in filePath:
        mc.file(
            f,
            open=True,
            force=True,
            loadReferenceDepth="none")
        referencePathreplace(oldpath, newpath)
        loadAllReferences()
        replaceMapPath(oldpath, newpath)
        mc.file(save=True)
    mc.evalDeferred("mc.file(save=True)")


def replaceMapPath(oldpath, newpath):
    fileNode = mc.ls(type="file")
    for f in fileNode:
        texPath = mc.getAttr("%s.fileTextureName" % f)
        newPath = texPath.replace(oldpath, newpath)
        if not texPath == newPath:
            print(texPath)
            colorSpace = mc.getAttr("%s.colorSpace" % f)
            mc.setAttr(
                "%s.fileTextureName" % f, newPath, type="string")
            mc.setAttr("%s.colorSpace" % f, colorSpace, type="string")


def loadAllReferences():
    '''
    Load all references
    '''
    rflist = mc.ls(rf=True)
    for i in rflist:
        req = mc.referenceQuery(i, il=True)
        print(req)
        if not req:
            print(i)
            mc.file(loadReference=i)
