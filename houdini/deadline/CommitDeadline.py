#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import os
import hou
import Deadline.DeadlineConnect as Connect


# hipPath = hou.hipFile.name()
# hipName = hou.hipFile.basename().split(".")[0]
# renderNode = hou.selectedNodes()[0]
# node = hou.selectedNodes()[0]
# start, end = node.parm("f1").eval(), node.parm("f2").eval()
# nodePath = node.path()
# UserName = os.getenv("USERNAME")
# listtype = [hipName, UserName, hipPath, start, end, nodePath]


def submitFile(
        hipName, UserName, hipPath, renderStart, renderend, MantraPath,
        theServer, port, renderGoupr, renderPool, perTask):
    '''
    pass
    '''
    Deadline = Connect.DeadlineCon(theServer, port)
    JobInfo = {
        "Name": hipName,
        "UserName": UserName,
        "Frames": "%d-%d" % (renderStart, renderend),
        "Plugin": "Houdini",
        "Priority": 100,
        "Pool": renderPool,
        "ChunkSize": perTask,  # Stand alone rendering frame
        "Group": renderGoupr
        }

    PluginInfo = {
        "Version": "18.0",
        "OutputDriver": MantraPath,
        "SceneFile": hipPath,
        }

    try:
        newJob = Deadline.Jobs.SubmitJob(JobInfo, PluginInfo)
        print("提交成功")
    except BaseException:
        print("Sorry, Web Service is currently down!")



# submitFile(hipName, UserName, hipPath, start, end, nodePath)
