#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import os
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QWidget
from PySide2.QtGui import QIntValidator
import PySide2.QtCore as QtCore
import Deadline.DeadlineConnect as Connect
import hou
from houdini.deadline import CommitDeadline
from qdarkstyle import style
reload(style)


osfile = os.path.split(__file__)[0]
uipath = os.path.join(osfile, "submitWinDCC.ui")
theServer = '192.168.5.11'
port = 8082


class submitUI(QWidget):

    def __init__(self, parent=None):
        super(submitUI, self).__init__(parent)
        # load UI File
        self.ui = QUiLoader().load(uipath, parentWidget=self)
        self.setWindowTitle("DeadlineSubmit")
        self.setParent(hou.ui.mainQtWindow(), QtCore.Qt.Window)

        self.Deadline = Connect.DeadlineCon(theServer, port)

        self.initIU()

        self.getOutNode()
        self.getGroup()

        # button command
        self.ui.RenderMantra.currentIndexChanged.connect(self.getRenderNode)
        self.ui.SubmitButton.clicked.connect(self.submitFile)

        self.setStyleSheet(style.qss)

    def initIU(self):

        # Limit the text box to the left
        self.ui.StartEdit.setAlignment(QtCore.Qt.AlignRight)
        self.ui.EndEdit.setAlignment(QtCore.Qt.AlignRight)

        # Limit text boxes to integers
        self.ui.StartEdit.setValidator(QIntValidator())
        self.ui.EndEdit.setValidator(QIntValidator())

        #
    def getOutNode(self):
        '''
        Get mantra in the scene
        '''
        outnode = hou.node("out/")
        self.children = outnode.children()
        mantraPath = [i.path() for i in self.children]

        self.ui.RenderMantra.addItems(mantraPath)

    def getRenderNode(self):
        '''
        Get Render mantra Node
        '''
        if self.ui.RenderMantra.currentText() == "selected Mantra":
            self.renderNode = hou.selectedNodes()[0]

        elif not self.ui.RenderMantra.currentText() == "None":
            self.renderNode = hou.node(self.ui.RenderMantra.currentText())

        start = int(self.renderNode.parm("f1").eval())
        end = int(self.renderNode.parm("f2").eval())
        self.ui.StartEdit.setText(str(start))
        self.ui.EndEdit.setText(str(end))

    def submitFile(self):
        # CommitDeadline.submitFile()
        if self.ui.RenderMantra.currentText() == "selected Mantra":
            node = hou.selectedNodes()[0]
            MantraPath = node.path()
        else:
            MantraPath = self.ui.RenderMantra.currentText()
        hipName = hou.hipFile.basename().split(".")[0]
        hipPath = hou.hipFile.name()
        UserName = os.getenv("USERNAME")
        renderStart = int(self.ui.StartEdit.text())
        renderend = int(self.ui.EndEdit.text())
        renderGoupr = self.ui.DeadlineGroups.currentText()
        renderPool = self.ui.DeadlinePools.currentText()
        perTask = self.ui.perTaskEdit.text()
        CommitDeadline.submitFile(
            hipName, UserName, hipPath, renderStart, renderend,
            str(MantraPath), theServer, port, str(renderGoupr),
            str(renderPool), perTask)

    def getGroup(self):
        '''
        get deadline GroupName and PoolName
        '''
        renderGoupr = self.Deadline.Groups.GetGroupNames()
        renderPool = self.Deadline.Pools.GetPoolNames()
        self.ui.DeadlineGroups.addItems(renderGoupr)
        self.ui.DeadlinePools.addItems(renderPool)


def runWin():
    run = submitUI()
    run.show()
